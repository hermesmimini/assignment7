package business;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import models.User;

@RequestScoped
@Path("/users")
public class UserRestService {
	
	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	public User getUserJson() {
		User user = new User();
		user.setFirstName("Hermes");
		user.setLastName("Mimini");
		return user;
	}
	
	@GET
	@Path("/get/{firstname}/{lastname}")
	@Produces(MediaType.APPLICATION_JSON)
	public User getUser(@PathParam("firstname") String firstname, @PathParam("lastname") String lastname) {
		
		//this would typically call a business service to search for a User but 
		//for now just print a debug message and echo back a default User
		System.out.println("API was /getuser: I recieved request for name: " + firstname + " " + lastname);
		
		User user = new User();
		user.setFirstName(firstname);
		user.setLastName(lastname);
		return user;
	}
	
	@POST
	@Path("/save")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public User saveUser(User user) {
		
		//this would typically call a business service to save a user
		//but for now just print a debug message and echo back the User
		System.out.println("Hello " + user.getFirstName() + " " + user.getLastName());
		return user;
	}
	
}
